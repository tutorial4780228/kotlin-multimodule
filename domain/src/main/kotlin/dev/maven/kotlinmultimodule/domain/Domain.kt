package dev.maven.kotlinmultimodule.domain

class Domain {
    fun getHello(name: String): String {
        return "Hello, $name!"
    }
}