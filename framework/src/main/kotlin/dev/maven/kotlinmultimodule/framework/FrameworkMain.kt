package dev.maven.kotlinmultimodule.framework

import dev.maven.kotlinmultimodule.domain.Domain

fun main() {
    val domain = Domain()

    print(domain.getHello("Blade"))
}